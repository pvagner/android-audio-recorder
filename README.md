# Audio Recorder

Android friendly!

Audio Recorder with custom recording folder, nice recording volume indicator, recording notification, recording lock screen activity.

# Manual install

    gradle installDebug

# Translate

If you want to translate 'Audio Recorder' to your language  please read this:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

# Screenshots

![shot](/docs/shot.png)

# Contributors

  * japanese translation thanks to @naofumi
  * german translation thanks to @vv01f
  * brazilian translation thanks to @vrozsas
  * itallizan tralslation thanks to @Agno94

# Donations

  * [PayPal EUR](https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=axet@me.com&amount=&currency_code=EUR&return=&item_name=audio-recorder)
  * [PayPal USD](https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=axet@me.com&amount=&currency_code=USD&return=&item_name=audio-recorder)
  * [PayPal RUB](https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=axet@me.com&amount=&currency_code=RUB&return=&item_name=audio-recorder)
